import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import TouchID from 'react-native-touch-id';

const App = () => {
  const optionalConfigObject = {
    title: 'Authentication Required',
    color: '#e00606',
    fallbackLabel: 'Show Passcode',
  };

  const pressHandler = () => {
    TouchID.authenticate(
      'to demo this react-native component',
      optionalConfigObject,
    )
      .then(success => {
        alert(success);
      })
      .catch(error => {
        alert('Authentication Failed');
      });
  };

  const clickHandler = () => {
    TouchID.isSupported()
      .then(biometryType => {
        if (biometryType === 'FaceID') {
          alert('FaceID is supported.');
        } else if (biometryType === 'TouchID') {
          alert('TouchID is supported.');
        } else if (biometryType === true) {
          alert(' Supported TouchId for android');
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <TouchableOpacity onPress={() => pressHandler()}>
        <Text> Authenticate with Touch ID </Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => clickHandler()}>
        <Text>Check is supported for biometryType</Text>
      </TouchableOpacity>
    </View>
  );
};

export default App;
